package pivottable.model;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;

import org.junit.Before;
import org.junit.Test;

import pivottable.io.CSVImporter;
import pivottable.model.PTCell;
import pivottable.model.PTData;
import pivottable.model.PTFieldSet;
import pivottable.model.PTFieldValue;
import pivottable.model.PTOperation;
import pivottable.model.PTSortOrder;
import pivottable.model.PivotTable;


public class PivotTableTest {

	PTCell testCell;
	PivotTable testTable;
	PivotTable testTable2;
	PTData pivotData;
	
	@Before
	public void PivotTableTest_setup() {
		//Import Data
		CSVImporter testImporter = CSVImporter.getInstance();
		PivotTable pt = new PivotTable();
		testImporter.importData("src/main/java/files/SalesJan2009.csv", pt);
		PTData pivotData = pt.getPTData();
		
		ArrayList<PTFieldSet> columnFields = new ArrayList<>();
		ArrayList<PTFieldSet> rowFields = new ArrayList<>();
		
		PTFieldSet fs1 = new PTFieldSet(3, PTSortOrder.ASC); //payment
		PTFieldSet fs2 = new PTFieldSet(1, PTSortOrder.ASC); //product	
		columnFields.add(fs1);
		rowFields.add(fs2);
			
		//2 = price ; 6 = columns per page; 20 = rows per page; 1 = current Page
	
		testTable = new PivotTable();
		testTable.setColumnFields( columnFields );
		testTable.setRowFields( rowFields );
		testTable.setAggregationField( 2 );
		testTable.setPTData( pivotData );
		testTable.pivotTableInit();
		
	}

	@Test
	public void pivotTable_is_not_null() {
		assertNotNull("The pivot table was created",testTable);	
	}
	
	@Test
	public void pivotTable_column_is_null() {
		assertNull("The pivot table was created",testTable2);	
	}
	
	@Test
	public void Summary_column_is_not_null() {
		testTable.createTotalsCol();
		assertNotNull("The summary column was created",testTable.getTotalsCol());	
	}
	 	
	@Test
	public void Summary_column_size_is_Correct() {
		testTable.createTotalsCol();
		assertEquals("",4,testTable.getTotalsColSize());
	}
	
	@Test
	public void check_Summary_Column_Value_Equals() {
		ArrayList<PTCell> test = testTable.getTotalsCol();		
		assertEquals("","188900",test.get(0).getValue());
	}
	
	@Test
	public void check_Summary_Column_Value_not_Equals() {
		ArrayList<PTCell> test = testTable.getTotalsCol();		
		assertNotEquals("","-188900",test.get(0).getValue());
	}
	
	@Test
	public void Summary_Row_is_not_null() {
		testTable.createTotalsRow();
		assertNotNull("The summary Row was created",testTable.getTotalsRow());	
	}
	 	
	@Test
	public void Summary_Row_size_is_Correct() {
		testTable.createTotalsRow();
		assertEquals("",3,testTable.getTotalsRowSize());
	}
	
	@Test
	public void check_Summary_Row_Value_Equals() {
		ArrayList<PTCell> test = testTable.getTotalsRow();		
		assertEquals("","1028400",test.get(0).getValue());
	}
	
	@Test
	public void check_Summary_Row_Value_not_Equals() {
		ArrayList<PTCell> test = testTable.getTotalsRow();		
		assertNotEquals("","-188900",test.get(0).getValue());
	}

}
