package pivottable.table;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import pivottable.model.PTData;
import pivottable.model.PTDataType;
import pivottable.model.PTField;


public class PTDataTest {
	
	@Test
	public void init_is_not_null () {
		String[] testString = {"Hello", "World"};
		PTDataType[] testFieldDatatypes = new PTDataType[]{PTDataType.STRING, PTDataType.STRING};
		PTData testData = new PTData();
		testData.setFieldNames( testString );
		assertNotNull("PTData init returns an object", testData);
	}
	
	@Test
	public void double_returns_double () {
		// create a test set
		String[] testSet = new String[]{"-1.0000034", "2.0", "4.0", "0.0"};
		assertEquals(PTDataType.DOUBLE, PTDataType.getTypeOfField(testSet).get());
	}
	
	@Test
	public void test_ignores_empty_entries () {
		String[] testSet = new String[]{"", "", "", "", "0"};
		assertEquals(PTDataType.INT, PTDataType.getTypeOfField(testSet).get());
	}
	
	@Test
	public void defaults_to_double_for_mixed_number_inputs () {
		String[] testSet = new String[]{"1", "1", "1.0"};
		assertEquals(PTDataType.DOUBLE, PTDataType.getTypeOfField(testSet).get());
	}
	
	@Test
	public void email_returns_email () {
		String[] testSet = new String[]{"test@test.com", "pivothero@pivothero.com"};
		assertEquals(PTDataType.EMAIL, PTDataType.getTypeOfField(testSet).get());
	}
	
	@Test
	public void int_returns_int () {
		String[] testSet = new String[]{"123", "456"};
		assertEquals(PTDataType.INT, PTDataType.getTypeOfField(testSet).get());
	}
	
	@Test
	public void date_returns_date () {
		String[] testSet = new String[]{"1/1/2001", "15/2/2017"};
		assertEquals(PTDataType.DATE, PTDataType.getTypeOfField(testSet).get());
	}
	
	@Test
	public void datetime_returns_datetime () {
		String[] testSet = new String[]{"2001-5-18 01:34:11", "2020-12-14 21:04:59"};
		assertEquals(PTDataType.DATETIME, PTDataType.getTypeOfField(testSet).get());
	}
	
	
	
	

}
