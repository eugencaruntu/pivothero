package pivottable.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pivottable.App;
import pivottable.model.PivotTable;

public class PHDExporter {

	private Stage primaryStage;
	private PivotTable pt;

	public PHDExporter( App app ) {
		primaryStage = app.getRootStage();
		pt = app.getPivotTable();
	}

	public void savePHD(String filePath) {
		try {
			FileOutputStream fileOut =
					new FileOutputStream( filePath );
			ObjectOutputStream out = new ObjectOutputStream( fileOut );
			out.writeObject( pt );
			out.close();
			fileOut.close();

			// confirmation message
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText("Serialized data file saved");
			alert.setContentText("Serialized data is saved in " + filePath );
			alert.showAndWait();

		} catch ( IOException i ) {
			// Display error message 	
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("Error");
			alert.setContentText(i.getLocalizedMessage());
			alert.showAndWait();		
		} 
	}

	public void drawUI() {

		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle( "Choose a destination" );
		// Set extension filter
		FileChooser.ExtensionFilter extFilter =
				new FileChooser.ExtensionFilter( "PHD files (*.phd)", "*.phd" );
		fileChooser.getExtensionFilters().add( extFilter );

		// Show save file dialog
		File file = fileChooser.showSaveDialog( primaryStage );

		if ( file != null ) {
			savePHD( file.getAbsolutePath() );
		} 

	}

}
