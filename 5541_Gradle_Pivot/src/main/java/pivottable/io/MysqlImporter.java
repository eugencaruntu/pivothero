package pivottable.io;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import pivottable.model.PTData;
import pivottable.model.PivotTable;

/**
 * Imports dataset from MySQL database, creates a PTData instance, populates the
 * Records in the PTData one row at a time. Before returning the PTData, it
 * intializes the PTData by indexing the fields and setting the field types.
 */
public class MysqlImporter implements Importer {

	/* Class constants */

	private final String JDBC_DRIVER = "com.mysql.jdbc.Driver";

	/* Constructors */

	/**
	 * Default Constructor
	 */
	public MysqlImporter() {}

	/* Class Methods */

	/*
	 * Attempt to open a connection using src as credentials.
	 * Returns true for a valid connection, false otherwise.
	 */
	@Override
	public boolean validCnx(String src) {
		// split the connection string:
		String cnx[] = src.split( "##" );
		String DB_URL = cnx[0];
		String USER = cnx[1];
		String PASS = cnx[2];

		try {
			// STEP 2: Register JDBC driver
			Class.forName( JDBC_DRIVER );

			// STEP 3: Open a connection
			DriverManager.getConnection( DB_URL, USER, PASS );
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("Failed to connect to database");
			alert.setContentText("There was an error connecting to database"
					+ "\nError was: \n" + e.getLocalizedMessage() + "\nPlease try again.");
			alert.showAndWait();
			return false;
		}
		return true;
	}

	/**
	 * Imports data from MySQL database using src as credentials
	 */
	@Override
	public boolean importData(String src, PivotTable pt) {

		PTData ptData = new PTData();

		// split the connection string:
		String cnx[] = src.split( "##" );
		String DB_URL = cnx[0];
		String USER = cnx[1];
		String PASS = cnx[2];
		String SQL = cnx[3];

		// patched together from :
		// https://www.tutorialspoint.com/jdbc/jdbc-sample-code.htm

		Connection conn = null;
		Statement stmt = null;
		try {
			// STEP 2: Register JDBC driver
			Class.forName( JDBC_DRIVER );

			// STEP 3: Open a connection
			conn = DriverManager.getConnection( DB_URL, USER, PASS );

			// STEP 4: Execute a query
			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery( SQL );

			String[] headerData = new String[rs.getMetaData().getColumnCount() - 1];

			// Generates headerData and headerTypes
			// TODO the 2 here is to get rid of the index part of the sql record
			for ( int i = 2; i <= rs.getMetaData().getColumnCount(); i++ ) {
				headerData[i - 2] = rs.getMetaData().getColumnName( i );
			}

			ptData.setFieldNames( headerData );

			// STEP 5: Extract data from result set
			while ( rs.next() ) {
				// create a string array for the record
				String[] data = new String[rs.getMetaData().getColumnCount() - 1];
				// add each record element to the array
				for ( int i = 2; i <= rs.getMetaData().getColumnCount(); i++ ) {
					data[i - 2] = rs.getString( i ).trim();
				}
				// Add the record
				ptData.addRecord( data );
			}

			// STEP 6: Clean-up environment
			rs.close();
			stmt.close();
			conn.close();

		} catch ( SQLException se ) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("SQL Statement Error");
			alert.setContentText("There was an error in your SQL statement."
					+ "\nError was: \n" + se.getLocalizedMessage() + "\nPlease try again.");
			alert.showAndWait();
			return false;


		} catch ( Exception e ) {
			// Handle errors for Class.forName
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("Class.forName");
			alert.setContentText(e.getLocalizedMessage());
			alert.showAndWait();

		} finally {
			// finally block used to close resources
			try {
				if ( stmt != null )
					stmt.close();
			} catch ( SQLException se2 ) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText("SQL Statement Closing Error");
				alert.setContentText("There was an error closing SQL statement."
						+ "\nError was: \n" + se2.getLocalizedMessage());
				alert.showAndWait();
			} // nothing we can do
			try {
				if ( conn != null )
					conn.close();
			} catch ( SQLException se ) {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText("Error when closing connection");
				alert.setContentText("There was an error when closing connection"
						+ "\nError was: \n" + se.getLocalizedMessage());
				alert.showAndWait();
			} // end finally try
		} // end try

		// finish by indexing fields and setting the header types
		ptData.indexFields();
		ptData.setFieldTypes();
		pt.setPTData( ptData );

		return true;

	}

}
