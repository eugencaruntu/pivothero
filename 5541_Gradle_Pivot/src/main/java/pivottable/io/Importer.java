package pivottable.io;

import pivottable.model.PivotTable;

/**
 * Interface for the PTData Importer
 */
public interface Importer {

    /**
     * The importData method takes a string representing the data source and a
     * PivotTable instance and imports data into the PivotTable's PTData
     * property.
     * 
     * @param src
     * @return true when pt.ptData is initialized
     */
    public boolean importData(String src, PivotTable pt);

    /**
     * The validCnx method takes a string representing the data source /
     * connection string parameters for a database and authenticates connection
     * if DB url and credentials are valid.
     * 
     * @param src
     * @return boolean
     */
    public boolean validCnx(String src);

}
