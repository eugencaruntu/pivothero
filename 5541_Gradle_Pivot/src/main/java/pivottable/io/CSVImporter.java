package pivottable.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import pivottable.model.PTData;
import pivottable.model.PivotTable;

/**
 * Imports CSV file, creates a PTData instance, populates the Records in the PTData
 * one row at a time. Before returning the PTData, it intializes the PTData by 
 * indexing the fields and setting the field types.
 */
public class CSVImporter implements Importer {

	/* Class attributes */

	private static CSVImporter instance;

	/* Constructors */

	/**
	 * Constructor
	 */
	private CSVImporter() {

	}

	/**
	 * Singleton
	 */
	public static synchronized CSVImporter getInstance() {
		if (instance == null)
			instance = new CSVImporter();
		return instance;
	}

	/* Class Methods */

	/**
	 * dbConnect method is not used for CSV import
	 */
	@Override
	public boolean validCnx(String src) {
		// Create a BufferedReader object to attempt to read file from src
		BufferedReader testRead = null;
		try {
			testRead = new BufferedReader(new FileReader( src ));
		} catch (Exception e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("Failed to locate CSV");
			alert.setContentText(e.getLocalizedMessage());
			alert.showAndWait();
			return false;
		} finally {
			if (testRead != null) {
				try {
					testRead.close();
				} catch (IOException e) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setHeaderText("Encountered an exception closing the file.");
					alert.setContentText(e.getLocalizedMessage());
					alert.showAndWait();
				}
			}
		}
		return true;
	}

	/**
	 * Imports data from CSV file using src string as file path
	 * @param src The file path for the csv to import
	 */
	@Override
	public boolean importData(String src, PivotTable pt) {

		PTData ptData = new PTData();

		CSVReader reader = null;
		try {
			reader = new CSVReader(new FileReader(src));
		} catch (FileNotFoundException e2) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("File Not Found. Check that you supplied a valid path.");
			alert.setContentText(e2.getLocalizedMessage());
			alert.showAndWait();

		}

		String[] headerData = null;
		String[] nextLine = null;

		try {
			headerData = reader.readNext();
		} catch (IOException e1) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("There's a problem reading this file's header. \nPlease check that the CSV is properly formatted.");
			alert.setContentText(e1.getLocalizedMessage());
			alert.showAndWait();
		}
		// Instantiate pivotData
		ptData.setFieldNames( trim(headerData) );

		try {
			while ((nextLine = reader.readNext()) != null) {
				if (nextLine.length > 1 && nextLine[0].length() > 1 && nextLine[0] != " "){
					nextLine = trim(nextLine);
					ptData.addRecord(nextLine);
				}
			}
		} catch (IOException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setHeaderText("There's a problem reading this file's records. \nPlease check that the CSV is properly formatted.");
			alert.setContentText(e.getLocalizedMessage());
			alert.showAndWait();
		} finally {
			if ( reader != null ) {
				try {
					reader.close();
				} catch ( IOException e ) {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setHeaderText("Encountered an exception closing the file.");
					alert.setContentText(e.getLocalizedMessage());
					alert.showAndWait();
				}
			}
		}

		// Finish by indexing fields and setting the header types
		ptData.indexFields();
		ptData.setFieldTypes();
		pt.setPTData( ptData );

		return true;

	}
	public String[] trim(String[] line){
		for (int i=0; i<line.length; i++) line[i] = line[i].trim();
		return line;
	}
}
