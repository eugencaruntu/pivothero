package pivottable.io;

public class ImporterFactory {

    public Importer getImporter(ImporterType type) {
        if (type == ImporterType.CSV) {
            // Singleton
            return CSVImporter.getInstance();
        } else if (type == ImporterType.MYSQL) {
            return new MysqlImporter();
        } else if (type == ImporterType.SQLSERVER) {
            return new MSsqlImporter();
        }
        return null;
    }
    
}
