package pivottable.io;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class PTFileWriter {

	/**
	 * Output string to file
	 * @param text
	 * @param file
	 */
	public static void writeFile(String text, String file) {

		try (BufferedWriter bw = new BufferedWriter(new FileWriter(file, true))) {
			bw.write(text);
			bw.newLine();

		} catch (IOException e) {
			e.printStackTrace();

		}

	}

}
