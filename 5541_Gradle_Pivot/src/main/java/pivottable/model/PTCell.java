package pivottable.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.OptionalDouble;
import java.util.OptionalInt;

/**
 * PivotCell takes a set of record ids and a reference to a PivotTable and
 * performs an aggregate calculation based on the PivotTable's operation and
 * aggregationField
 */
public class PTCell implements Serializable {

    /* For serializing */
    private static final long serialVersionUID = 2632541838467100010L;
    
    /* Class attributes */

    /** 
     * The PivotTable data source for calculations 
     */
    private PivotTable pivotTable;

    /** 
     * The set of ids calculated by PivotTable outer class 
     */
    private HashSet<Integer> ids;

    /** 
     * The calculated value of the cell 
     */
    private String value;

    /* Constructors */

    /**
     * Private constructor takes a pivotTable instance and hashset of cells
     * 
     * @param pivotTable The data-set
     * @param ids The set of IDs to use in calculating the cell value
     */
    private PTCell( PivotTable pivotTable, HashSet<Integer> ids ) {
        this.pivotTable = pivotTable;
        this.ids = ids;
        this.value = "";
    }

    /* Class Methods */

    /**
     * Public factory method to return initialized PTCell
     * 
     * @param pivotTable
     * @param ids
     * @return
     */
    public static PTCell create(PivotTable pivotTable, HashSet<Integer> ids) {
        PTCell cell = new PTCell( pivotTable, ids );
        cell.calculateValue();
        return cell;
    }

    /**
     * Method that computes the value of the cell based on the pivotTable's
     * operation type and aggregationField
     */
    public void calculateValue() {
        // TODO needs case statement to get operation type from pivotTable
        switch (pivotTable.getOperationType()) {
        case SUM:
            value = sum();
            break;
        case COUNT:
            value = count();
            break;
        case MEAN:
            value = mean();
            break;
        case MAXIMUM:
        	value = max();
        	break;
        case MINIMUM:
        	value = min();
        	break;
        default: // default calculateValue is the sum operation
            value = sum();
            break;
        }
    }

    /* Aggregation functions */

    /**
     * Sums the values of the cell's ids
     */
    private String sum() { // TODO check for data type of aggregation field
        /* For each id in ids, add the aggregation */

        PTDataType fieldType = pivotTable.getPTData()
                .getField( pivotTable.getAggregationField() ).getDataType();

        if ( fieldType == PTDataType.INT )
            return Integer.toString( sumInt() ); // returns integer sum
        if ( fieldType == PTDataType.DOUBLE )
            return Double.toString( sumDouble() ); // returns double sum
        else
            return Integer.toString( sumString() ); // return integer sum of all
                                                    // string lengths
    }

    /**
     * Sums the values of the cell's ids if aggregation field is int
     * 
     * @return int
     */
    private int sumInt() {
        return ids.stream().mapToInt( (e) -> Integer.parseInt( pivotTable.getPTData()
                .getRecord( e )[pivotTable.getAggregationField()] ) ).sum();
    }

    /**
     * Sums the values of the cell's ids if aggregation field is double
     * 
     * @return double
     */
    private double sumDouble() {
        return ids.stream().mapToDouble( (e) -> Double.parseDouble( pivotTable
                .getPTData().getRecord( e )[pivotTable.getAggregationField()] ) )
                .sum();
    }

    /**
     * Sums the values of the cell's ids if aggregation field is String, using
     * the length of the String as the value to sum.
     * 
     * @return int
     */
    private int sumString() {
        return ids
                .stream().mapToInt( (e) -> pivotTable.getPTData()
                        .getRecord( e )[pivotTable.getAggregationField()].length() )
                .sum();
    }

    /**
     * Returns the number of IDs
     */
    private String count() {
        return Integer.toString( ids.size() );
    }
    
    /**
     * Returns the maximum of the values
     */
    private String max() {
    	/* For each id in ids, add the aggregation */
    	
    	PTDataType fieldType = pivotTable.getPTData()
                .getField( pivotTable.getAggregationField() ).getDataType();
    	        
        // returns integer max
        if ( fieldType == PTDataType.INT ) {
        	//For each id in ids, get the maximum as OptionalInt
            OptionalInt oi = ids.stream().mapToInt( (e) -> Integer.parseInt( pivotTable.getPTData()
                    .getRecord( e )[pivotTable.getAggregationField()] ) ).max();
            if (oi.isPresent()) 
            	return Integer.toString(oi.getAsInt());
            else
            	return "";
        }
        // returns double max
        else if ( fieldType == PTDataType.DOUBLE ) {
            double max = maxDouble();
            return Double.isNaN( max ) ? "" : Double.toString(max);
        }
        // returns string length max
        else {
        	OptionalInt oi = ids.stream().mapToInt( (e) -> pivotTable.getPTData()
                    .getRecord( e )[pivotTable.getAggregationField()].length() ).max();
        	if (oi.isPresent())
        		return Integer.toString(oi.getAsInt());
        	else
        		return "";
        }
        
    }
    
    /**
     * Returns the minimum of the values
     */
    private String min() {
    	/* For each id in ids, add the aggregation */
    	
    	PTDataType fieldType = pivotTable.getPTData()
                .getField( pivotTable.getAggregationField() ).getDataType();
    	        
        // returns integer min
        if ( fieldType == PTDataType.INT ) {
        	//For each id in ids, get the minimum as OptionalInt
            OptionalInt oi = ids.stream().mapToInt( (e) -> Integer.parseInt( pivotTable.getPTData()
                    .getRecord( e )[pivotTable.getAggregationField()] ) ).min();
            if (oi.isPresent()) 
            	return Integer.toString(oi.getAsInt());
            else
            	return "";
        }
        // returns double min
        else if ( fieldType == PTDataType.DOUBLE ) {
            double min = minDouble();
            return Double.isNaN( min ) ? "" : Double.toString(min);
        }
        // returns string length min
        else {
        	OptionalInt oi = ids.stream().mapToInt( (e) -> pivotTable.getPTData()
                    .getRecord( e )[pivotTable.getAggregationField()].length() ).min();
        	if (oi.isPresent())
        		return Integer.toString(oi.getAsInt());
        	else
        		return "";
        }
    }
    

    /**
     * Finds the maximum value of the cell's ids if aggregation field is double
     * 
     * @return double
     */
    private double maxDouble() {
    	OptionalDouble od = ids.stream().mapToDouble( (e) -> Double.parseDouble( pivotTable
                .getPTData().getRecord( e )[pivotTable.getAggregationField()] ) )
                .max();
    	if (od.isPresent())
    		return od.getAsDouble();
    	else 
    		return Double.NaN;
    }
    
    /**
     * Finds the minimum value of the cell's ids if aggregation field is double
     * 
     * @return double
     */
    private double minDouble() {
    	OptionalDouble od = ids.stream().mapToDouble( (e) -> Double.parseDouble( pivotTable
                .getPTData().getRecord( e )[pivotTable.getAggregationField()] ) )
                .min();
    	if (od.isPresent())
    		return od.getAsDouble();
    	else 
    		return Double.NaN;
    }


    /**
     * Calculates the mean of the values
     */
    private String mean() {

        PTDataType fieldType = pivotTable.getPTData()
                .getField( pivotTable.getAggregationField() ).getDataType();
        
        double mean;
        
        // returns integer mean
        if ( fieldType == PTDataType.INT )
            mean = sumInt() / (double) ids.size();     
        // returns double mean
        else if ( fieldType == PTDataType.DOUBLE )
            mean = sumDouble() / ids.size();
        // returns string length mean
        else
            mean = sumString() / (double) ids.size();
        // return empty string if NaN else the mean
        return Double.isNaN( mean ) ? " " : Double.toString( mean );

    }

    /* Getter and setter methods */

    /**
     * @return the cellValue
     */
    public String getValue() {
        return value;
    }

}