package pivottable.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * The PivotData data-structure contains the data to be accessed by the
 * PivotTable and includes additional data-structures and methods required for
 * creating, and computing the PivotTable.
 */
public class PTData implements Serializable {

    /* For serializing */    
    private static final long serialVersionUID = -2464446402522066422L;

    
    /* Class attributes */

    /** Holds the individual records from the imported data-source */
    private HashMap<Integer, String[]> records;

    /** Holds count of records */
    private int numRecords = 0;

    /**
     * Each Field represents a column of the imported data-source. Each Field
     * contains the field name, all its unique values, and a set of IDs for each
     * value.
     */
    private ArrayList<PTField> fields;

    /** Holds count of fields */
    private int numFields;

    /* Constructors */
    
    /**
     * PTData constructor with no args
     */
    public PTData() {
        numFields = 0;
        fields = new ArrayList<PTField>();
        records = new HashMap<Integer, String[]>();
    }
    

    /* Class Methods */

    /* Records */

    /**
     * Adds a new record to the record list
     * 
     * @param data An array of strings representing the fields in a single
     *            record
     */
    public void addRecord(String[] data) {
        getRecords().put( numRecords, data );
        numRecords++;
    }

    /**
     * Gets reference to the record set
     * 
     * @return A reference to the records
     */
    public HashMap<Integer, String[]> getRecords() {
        return records;
    }

    /**
     * Gets the record corresponding to the recordID
     * 
     * @param recordId ID of record
     * @return String array corresponding to single record
     */
    public String[] getRecord(int recordId) {
        return records.get( recordId );
    }
    
    /**
     * Prints initial records including headers
     */
    public String getRecordsString() {
        StringBuilder output = new StringBuilder();

        // Build header
        for (int g = 0; g < numFields; g++){
            output.append( String.format( "%-30s", getFieldName(g) ));
        }
        // Line
        output.append( "\n" );
        for (int h = 0; h < numFields; h++){
            output.append( "------------------------------" );
        }
        output.append( "\n" );
        // The records
        for (int i = 0; i < numRecords; i++) {
            String[] record = records.get( i );
            for (int j = 0; j < numFields; j++) {
                output.append( String.format( "%-30s", record[j] ) );
            }
            output.append( "\n" );
        }
        
        return output.toString();
    }

    /**
     * Gets the number of records in the record list
     * 
     * @return The number of records
     */
    public int getNumRecords() {
        return numRecords;
    }

    /* Fields */
    
    /**
     * Sets field names
     */
    public void setFieldNames(String[] fieldNames){
        this.numFields = fieldNames.length;
        for ( String fieldName : fieldNames ) {
            addField( new PTField( fieldName, PTDataType.STRING ) );
        }
    }
    

    /**
     * Adds a new field entry to fields collection.
     * 
     * @param field Field to add to fields collection
     */
    public void addField(PTField field) {
        fields.add( field );
    }
    
    /**
     * Determines Field Types
     */
    public void setFieldTypes() {
        int fieldCounter = 0;
        for (PTField field : getFields()){
            String[] list = new String[10];
            // make array per column for the first 10 records in the set
            for (int i = 1; i < 11; i++) {          
                list[i-1] = getRecord(i)[fieldCounter];   
            }
            // get the datatype for the column
            field.setDataType( PTDataType.getTypeOfField(list).get() );
            fieldCounter++;
        }
    }
    

    /**
     * Creates and returns a list of field names from fields collection.
     * 
     * @return Returns an ordered list of field names
     */
    public ArrayList<String> getFieldNames() {
        ArrayList<String> fieldNames = new ArrayList<String>();
        // Build fieldNames from Fields in fields
        for ( PTField field : fields ) {
            fieldNames.add( field.getName() );
        }
        // Return the list
        return fieldNames;
    }

    /**
     * Gets name of field corresponding to the fieldIndex
     * 
     * @param fieldIndex The index of the field in the fields collection
     * @return The name of the field corresponding to the fieldIndex
     */
    public String getFieldName(int fieldIndex) {
        return getFieldNames().get( fieldIndex );
    }
    
    /**
     * Creates and returns a list of field names from fields collection.
     * 
     * @return Returns an ordered list of field names
     */
    public ArrayList<String> getFieldLabels() {
        ArrayList<String> fieldLabels = new ArrayList<String>();
        // Build fieldNames from Fields in fields
        for ( PTField field : fields ) {
            fieldLabels.add( field.getLabel() );
        }
        // Return the list
        return fieldLabels;
    }
    
    /**
     * Gets label of field corresponding to the fieldIndex
     * 
     * @param fieldIndex The index of the field in the fields collection
     * @return The label of the field corresponding to the fieldIndex
     */
    public String getFieldLabel(int fieldIndex) {
        return getFieldLabels().get( fieldIndex );
    }
    
    /**
     * Sets label of field corresponding to the fieldIndex
     * 
     * @param fieldIndex The index of the field in the fields collection
     * @param label The label name to set
     * @return The label of the field corresponding to the fieldIndex
     */
    public void setFieldLabel(int fieldIndex, String label) {
        getField(fieldIndex).setLabel( label );
    }

    /**
     * Gets reference to fields object
     * 
     * @returns ArrayList<Field> of all fields
     */
    public ArrayList<PTField> getFields() {
        return fields;
    }

    /**
     * Gets Field corresponding to the fieldIndex
     * 
     * @param fieldIndex The index of the field in the fields collection
     * @return The field corresponding to the fieldIndex
     */
    public PTField getField(int fieldIndex) {
        return fields.get( fieldIndex );
    }

    /**
     * Gets list of field values names in alphabetical order for a given
     * fieldIndex
     * 
     * @param fieldIndex The index of the field in the fields collection
     * @return ArrayList<String> of field values for a given fieldIndex
     */
    public ArrayList<String> getFieldValuesAlpha(int fieldIndex, PTSortOrder sortOrder) {
        return fields.get( fieldIndex ).getValuesAlpha(sortOrder);
    }

    /**
     * Gets set of field IDs for a given fieldIndex and fieldValue
     * 
     * @param fieldIndex The index of the field in the fields collection
     * @param fieldValue The name of the field value whose IDs should be
     *            retrieved
     */
    public HashSet<Integer> getFieldIdsByField(int fieldIndex, String fieldValue) {
        return getField( fieldIndex ).getFieldIds( fieldValue );
    }

    /**
     * Iterates through all records and indexes each record by all its fields,
     * storing this information in the fields data-structure.
     */
    public void indexFields() {
        // For each record in the record set
        for ( int recordId : getRecords().keySet() ) {
            String[] record = getRecord( recordId );
            /*
             * Iterate through record and add recordId to set for each of its
             * field values
             */
            for ( int fieldId = 0; fieldId < numFields; fieldId++ ) {
                // If the set for the value already exists, just add to it
                if ( getField( fieldId ).getValues().containsKey( record[fieldId] ) ) {
                    getField( fieldId ).addValueId( record[fieldId], recordId );
                }
                else {
                    /*
                     * Make the set, add the record key, then add the set to the
                     * HashMap using the value as key
                     */
                    HashSet<Integer> ids = new HashSet<>();
                    ids.add( recordId );
                    getField( fieldId ).addValue( record[fieldId], ids );
                }
            }
        }
    }

    /**
     * Gets a set of record IDs corresponding to the intersection of two field
     * values' ID sets.
     * 
     * @param field1Index Index of first field in Fields
     * @param field1Value Value of first field
     * @param field2Index Index of second field in Fields
     * @param field2Value Value of second field
     * @return HashSet<Integer> of intersection of two fields
     */
    public HashSet<Integer> getIntersection(int field1Index, String field1Value,
            int field2Index, String field2Value) {
        HashSet<Integer> intersectionSet = new HashSet<Integer>(
                getField( field1Index ).getFieldIds( field1Value ) );
        intersectionSet.retainAll( getField( field2Index ).getFieldIds( field2Value ) );
        return intersectionSet;
    }

    /**
     * Gets a set of record IDs corresponding to the intersection of a set and
     * the set field value.
     * 
     * @param field1IdSet Set of IDs for first field value
     * @param field2Index Index of second field in Fields
     * @param field2Value Value of second field
     * @return HashSet<Integer> of intersection of two fields
     */
    public HashSet<Integer> getIntersection(HashSet<Integer> field1IdSet, int field2Index,
            String field2Value) {
        HashSet<Integer> intersectionSet = new HashSet<Integer>(
                getField( field2Index ).getFieldIds( field2Value ) );
        intersectionSet.retainAll( field1IdSet );
        return intersectionSet;
    }

    /**
     * Gets a set of record IDs corresponding to the intersection of two field
     * value ID sets
     * 
     * @param field1IdSet Set of IDs for first field value
     * @param field2IdSet Set of IDs for first field value
     * @return HashSet<Integer> of intersection of two fields
     */
    public HashSet<Integer> getIntersection(HashSet<Integer> field1IdSet,
            HashSet<Integer> field2IdSet) {
        // Create a new set from field1IdSet since set passed by reference is
        // mutable!
        HashSet<Integer> intersectionSet = new HashSet<>( field1IdSet );
        intersectionSet.retainAll( field2IdSet );
        return intersectionSet;
    }

}
