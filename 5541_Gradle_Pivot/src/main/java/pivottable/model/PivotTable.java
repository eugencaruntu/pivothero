package pivottable.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;

/**
 * The PivotTable data-structure represents the data model for the pivot table.
 */
public class PivotTable implements Serializable, Cloneable {
    
    /* For serializing */
    private static final long serialVersionUID = 9039602488262056364L;
   
    /* Class constants */
 
    private int max_cols = 14;
    private int max_rows = 22;

    /* Class attributes */

    /**
     * A list of PTFieldSets representing the columnField parameters in order of
     * segmentation
     */
    private ArrayList<PTFieldSet> columnFields;

    /**
     * A list of PTFieldSets representing the rowField parameters in order of
     * segmentation
     */
    private ArrayList<PTFieldSet> rowFields;

    /**
     * The fieldIndex for the field to be used for the aggregation calculations.
     */
    private int aggregationField;

    /**
     * The specified aggregation operation for the pivot table to perform
     */
    private PTOperation operationType;

    /**
     * A HashMap of filter parameters where the key corresponds to the filter
     * field index and the value corresponds to the name of the value.
     */
    private ArrayList<PTFieldValue> filterFieldValues;

    /**
     * Tree structure that represents the column fields and their segments used
     * in seeding the cells of the table
     */
    private TableHeaderTree columnHeaderTree;

    /**
     * Tree structure that represents the row fields and their segments used in
     * seeding the cells of the table
     */
    private TableHeaderTree rowHeaderTree;

    /**
     * 2D Array representing the table column headers and colspans
     */
    private ArrayList<ArrayList<THTLabel>> colHeadStruct;

    /**
     * 2D Array representing the table row headers and rowspans
     */
    private ArrayList<ArrayList<THTLabel>> rowHeadStruct;

    /**
     * Stores number of columns in pivot table
     */
    private int tableColSize;

    /**
     * Stores number of rows in pivot table
     */
    private int tableRowSize;

    /**
     * The columns per page
     */
    private int colsPerPage;

    /**
     * The rows per page
     */
    private int rowsPerPage;
    
    /**
     * The current start column for the current page
     */
    private int pageColStart;
    
    /**
     * The current start row for the current page
     */
    private int pageRowStart;

    /**
     * The current page
     */
    private int currentPage;

    /**
     * The total number of pages
     */
    private int totalNumPages;
    
    /**
     * Flag set to true if headers or paging parameters need to be rebuilt
     */
    private boolean rebuild;
    
    /**
     * Array representing cells in pivotTable
     */
    private PTCell[] ptCells;

    /**
     * The data-source for the PivotTable
     */
    private PTData ptData;
    
    /**
     * The data-structure for storing the PivotTable summary column
     */
    private ArrayList<PTCell> totalsCol;
    
    /**
     * The data-structure for storing the PivotTable summary row
     */
    private ArrayList<PTCell> totalsRow;

    /**
     * Grand Total Aggregation
     */
    private PTCell sumTotal;
    
    
    /* Constructors */
    
    /**
     * Creates a PivotTable data-structure and instantiates all internal structures
     */
    public PivotTable() {
        columnFields = new ArrayList<PTFieldSet>();
        rowFields = new ArrayList<PTFieldSet>();
        aggregationField = -1;
        operationType = PTOperation.SUM;
        filterFieldValues = new ArrayList<PTFieldValue>();
        ptData = new PTData();
        colsPerPage = 6;
        rowsPerPage = 15;
        currentPage = 1;
        tableColSize = 1;
        tableRowSize = 1;
        totalNumPages = 1;
        rebuild = true;
    }

    /* Class methods */

    /**
     * PivotTable Init builds the columnHeaderTree and rowHeaderTree and calculates
     * the number of pages for the table
     */
    public void pivotTableInit() {
        if (rebuild) {
            // Initialize header trees
            columnHeaderTree = new TableHeaderTree();
            rowHeaderTree = new TableHeaderTree();
            // Build the tree structure for the column and row headers
            columnHeaderTree.buildTree( columnFields, this );
            rowHeaderTree.buildTree( rowFields, this );
            // Create the structure used to render the segmented column headers
            colHeadStruct = columnHeaderTree.createHeaderLists();
            // Create the structure used to render the segmented row headers
            rowHeadStruct = rowHeaderTree.createHeaderLists();
            // Create the cell structure
            ptCells = initPTCells();
            // Create totalsCol
            createTotalsCol();
            // Create totalsRow
            createTotalsRow();
            // Create sumTotal
            sumTotal = PTCell.create( this, columnHeaderTree.getRoot().getFieldIds() );
            // Set current page to 1
            currentPage = 1;
            // Calculates the total number of pages based on paging parameters
            calculateNumPages();
            // Set the pageColStart and pageRowStart params
            setPageStart();
            // Set rebuild to false
            rebuild = false;
        } else {
            refreshCells();
        }
    }

    /* Pivot Cells */

    /**
     * Creates an array of PTCells to store table
     */
    private PTCell[] initPTCells() {

        tableRowSize = getRowHeaderTree().getLeafNodes().size();
        tableColSize = getColumnHeaderTree().getLeafNodes().size();

        PTCell[] ptCells = new PTCell[tableRowSize * tableColSize];

        int count = 0;
        
        ArrayList<PTCellThread> cellThreads = new ArrayList<>();

        for ( THTNode row : getRowHeaderTree().getLeafNodes() ) {

            /*
             * Gets the intersection of ids from the column and row fields in
             * the TableHeaderTrees and passes that set along with a reference
             * to the PivotTable to instantiate and initialize the PTCell.
             */
            for ( THTNode column : getColumnHeaderTree().getLeafNodes() ) {

                PTCellThread cellThread = new PTCellThread(count, row, column, ptCells);
                cellThreads.add( cellThread );
                cellThread.start();
                
                count++;
                
            }
        }
        
        for (PTCellThread thread : cellThreads) {
            try {
                thread.join();
            } catch ( InterruptedException e ) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
        return ptCells;
    }
    
    /**
     * Thread that creates cells
     */
    class PTCellThread extends Thread {
        
        private int id;
        private THTNode row;
        private THTNode column;
        private PTCell[] ptCells;
        
        public PTCellThread(int id, THTNode row, THTNode column, PTCell[] ptCells) {
            this.id = id;
            this.row = row;
            this.column = column;
            this.ptCells = ptCells;
        }
        
        public void run() {
                        
            HashSet<Integer> intersectionSet = getPTData()
                    .getIntersection( column.getFieldIds(), row.getFieldIds() );
            // Pass PTCell reference to current pivotTable
            PTCell cell = PTCell.create( PivotTable.this, intersectionSet );
            this.ptCells[id] = cell;
                        
        }
        
    }
    
    /**
     * Refreshes the cells if only the operation or field has been changed
     */
    public void refreshCells() {
        // refresh table cells
        for ( PTCell cell : ptCells ) {
            cell.calculateValue();
        }
        // refresh totals sets
        for ( PTCell cell : totalsCol ) {
            cell.calculateValue();
        }
        for ( PTCell cell : totalsRow ) {
            cell.calculateValue();
        }
        // refresh sumTotal
        sumTotal.calculateValue();
    }

    /**
     * Gets value of cell at row and column index
     */
    public String getCellAt(int row, int col) {
        int index = row * tableColSize + col;
        String value = "";
        try {
            value = ptCells[index].getValue();
        } catch ( Exception e ) {
            value = "###";
        }
        return value;
    }

    /**
     * Calculates the number of pages
     */
    private void calculateNumPages() {
        int horizontalPages = (int) Math
                .ceil( ((double) this.tableColSize / (double) this.colsPerPage) );
        int verticalPages = (int) Math
                .ceil( ((double) this.tableRowSize / (double) this.rowsPerPage) );
        this.totalNumPages = horizontalPages * verticalPages;
    }

    /**
     * Calculates the start indices for row and column for current page
     */
    private void setPageStart(){
        // the effective table size in columns is an even mulitplier of colsPerPage
        int effectiveTableColSize = (int)Math.ceil( (double) tableColSize / colsPerPage ) * colsPerPage;
        // calculates the pageColStart for the given page (uses currentPage-1 because
        // currentPage is 1-indexed
        pageColStart = ( ( (currentPage-1) * colsPerPage ) % effectiveTableColSize );
        // calculates the pageRowStart for the given page
        pageRowStart = (int) (rowsPerPage * Math.floor( (currentPage-1) / ( effectiveTableColSize / colsPerPage ) ));
    }

    /**
     * Creates a Summary Total Column 
     * TODO can refactor createTotalsCol() and createTotalsRow() into one method
     * that's passed a TableHeaderTree and returns an ArrayList<PTCell>
     * @return 
     */
    public void createTotalsCol() {
    	    	
    	ArrayList<PTCell> summaryCol = new ArrayList<>();
    	ArrayList<THTNode> leaves = this.getColumnHeaderTree().getLeafNodes();
    	
    	// for each leaf, create a new PTCell and push that into the summaryCol
    	for (THTNode leaf : leaves) {
    		PTCell summaryCell = PTCell.create(this, leaf.getFieldIds());
    		summaryCol.add(summaryCell);
    	}
    	totalsCol = summaryCol;
    }
    
    /**
     * Creates a Summary Total Row
     * TODO can refactor createTotalsCol() and createTotalsRow() into one method
     * that's passed a TableHeaderTree and returns an ArrayList<PTCell>
     * @return 
     */
    public void createTotalsRow() {
    	    	
    	ArrayList<PTCell> summaryRow = new ArrayList<>();
    	ArrayList<THTNode> leaves = this.getRowHeaderTree().getLeafNodes();
    	
    	// for each leaf, create a new PTCell and push that into the summaryCol
    	for (THTNode leaf : leaves) {
    		PTCell summaryCell = PTCell.create(this, leaf.getFieldIds());
    		summaryRow.add(summaryCell);
    	}
    	totalsRow = summaryRow;
    }
    
    /**
     * Returns PTCell of sumTotal
     */
    public PTCell getSumTotal() {
        return sumTotal;
    }
    
    /**
     * Setter Method for setting the max number of rows to print
     * @param int
     */
    public void setMaxRow(int maxRows) {
    	this.max_rows = maxRows;
    }
    
    /**
     * Setter Method for setting the max number of columns to print
     * @param int
     */
    public void setMaxCol(int maxCols) {
    	this.max_cols = maxCols;
    }
    
    /**
     * Getter Method for totalsCol
     * @return 
     */
    public ArrayList<PTCell> getTotalsCol() {
    	return this.totalsCol;
    }
    
    /**
     * Getter Method for totalsCol size
     * @return 
     */
    public int getTotalsColSize() {
    	return this.totalsCol.size();
    }
    
    /**
     * Getter Method for totalsRow
     * @return 
     */
    public ArrayList<PTCell> getTotalsRow() {
    	return this.totalsRow;
    }
    
    /**
     * Getter Method for totalsRow size
     * @return 
     */
    public int getTotalsRowSize() {
    	return this.totalsRow.size();
    }
    
    /* Getter and setter methods */

    /**
     * @return the aggregationField
     */
    public int getAggregationField() {
        return aggregationField;
    }

    /**
     * @param aggregationField the aggregationField to set
     */
    public void setAggregationField(int aggregationField) {
        this.aggregationField = aggregationField;
    }

    /**
     * @return the columnFields
     */
    public ArrayList<PTFieldSet> getColumnFields() {
        return columnFields;
    }

    /**
     * @param columnFields the columnFields to set
     */
    public void setColumnFields(ArrayList<PTFieldSet> columnFields) {
        // Setting new filters requires a rebuild
        if (!this.columnFields.equals( columnFields )) {
            this.rebuild = true;
        }
        this.columnFields = columnFields;
    }

    /**
     * @return the rowFields
     */
    public ArrayList<PTFieldSet> getRowFields() {
        return rowFields;
    }

    /**
     * @param rowFields the rowFields to set
     */
    public void setRowFields(ArrayList<PTFieldSet> rowFields) {
        // Setting new filters requires a rebuild
        if (!this.rowFields.equals( rowFields )){
            this.rebuild = true;
        }
        this.rowFields = rowFields;
    }

    /**
     * @return the operationType
     */
    public PTOperation getOperationType() {
        return operationType;
    }

    /**
     * @param operationType the operationType to set
     */
    public void setOperationType(PTOperation operationType) {
        this.operationType = operationType;
    }

    /**
     * @return the filterFieldValues
     */
    public ArrayList<PTFieldValue> getFilterFieldValues() {
        return filterFieldValues;
    }

    /**
     * @param filterFieldValues the filterFieldValues to set
     */
    public void setFilterFieldValues(ArrayList<PTFieldValue> filterFieldValues) {
        // Setting new filters requires a rebuild
        if (!filterFieldValues.equals( this.filterFieldValues )) {
            this.rebuild = true;
        }
        this.filterFieldValues = filterFieldValues;
    }

    /**
     * @return the colsPerPage
     */
    public int getColsPerPage() {
        return colsPerPage;
    }

    /**
     * @param colsPerPage the colsPerPage to set
     */
    public void setColsPerPage(int colsPerPage) {
        if (colsPerPage > max_cols) {
            if (this.colsPerPage != max_cols) {
                this.colsPerPage = max_cols;
                rebuild = true;
            }
        } else if (colsPerPage < 1) {
            if (this.colsPerPage != 1) {
                this.colsPerPage = 1;
                rebuild = true;
            }
            this.colsPerPage = 1;
        } else {
            if (this.colsPerPage != colsPerPage){
                this.colsPerPage = colsPerPage;
                rebuild = true;
            }
        }
    }

    /**
     * @return the rowsPerPage
     */
    public int getRowsPerPage() {
        return rowsPerPage;
    }

    /**
     * @param rowsPerPage the rowsPerPage to set
     */
    public void setRowsPerPage(int rowsPerPage) {
        if (rowsPerPage > max_rows) {
            if (this.rowsPerPage != max_rows) {
                this.rowsPerPage = max_rows;
                rebuild = true;
            }
        } else if (rowsPerPage < 1) {
            if (this.rowsPerPage != 1) {
                this.rowsPerPage = 1;
                rebuild = true;
            }
            this.rowsPerPage = 1;
        } else {
            if (this.rowsPerPage != rowsPerPage){
                this.rowsPerPage = rowsPerPage;
                rebuild = true;
            }
        }
    }

    /**
     * @return the currentPage
     */
    public int getCurrentPage() {
        return currentPage;
    }

    /**
     * @param currentPage the currentPage to set
     */
    public void setCurrentPage(int currentPage) {
        int page = currentPage;
        if (currentPage > this.totalNumPages ) {
            page = totalNumPages;
        } 
        if (currentPage < 1) {
           page = 1;
        } 
        // set current page attribute
        this.currentPage = page;
        // once the new page is set, set the pageColStart and pageRowStart attributes
        this.setPageStart();
    }

    /**
     * @return the totalNumPages
     */
    public int getTotalNumPages() {
        return totalNumPages;
    }

    /**
     * @param totalNumPages the totalNumPages to set
     */
    public void setTotalNumPages(int totalNumPages) {
        this.totalNumPages = totalNumPages;
    }

    /**
     * @return the columnHeaderTree
     */
    public TableHeaderTree getColumnHeaderTree() {
        return columnHeaderTree;
    }

    /**
     * @return the rowHeaderTree
     */
    public TableHeaderTree getRowHeaderTree() {
        return rowHeaderTree;
    }

    /**
     * @return the ptData
     */
    public PTData getPTData() {
        return ptData;
    }

    /**
     * sets the ptData
     */
    public void setPTData(PTData ptData){
        this.ptData = ptData;
    }
    
    /**
     * @return the colHeadStruct
     */
    public ArrayList<ArrayList<THTLabel>> getColHeadStruct() {
        return colHeadStruct;
    }

    /**
     * @return the rowHeadStruct
     */
    public ArrayList<ArrayList<THTLabel>> getRowHeadStruct() {
        return rowHeadStruct;
    }

    /**
     * @return the tableColSize
     */
    public int getTableColSize() {
        return tableColSize;
    }

    /**
     * @return the tableRowSize
     */
    public int getTableRowSize() {
        return tableRowSize;
    }

    /**
     * @return the pageColStart
     */
    public int getPageColStart() {
        return pageColStart;
    }

    /**
     * @param pageColStart the pageColStart to set
     */
    public void setPageColStart(int pageColStart) {
        this.pageColStart = pageColStart;
    }

    /**
     * @return the pageRowStart
     */
    public int getPageRowStart() {
        return pageRowStart;
    }

    /**
     * @param pageRowStart the pageRowStart to set
     */
    public void setPageRowStart(int pageRowStart) {
        this.pageRowStart = pageRowStart;
    }

    /**
     * @return the rebuild
     */
    public boolean mustRebuild() {
        return rebuild;
    }

    /* For cloning */
    /**
     * Overriding clone() method to create a deep copy of the PivotTable object.
     * @return Object to be cloned
     */
    public Object clone() throws CloneNotSupportedException
    {
        PivotTable ptclone = (PivotTable) super.clone();
        return ptclone;
    }
    
}
