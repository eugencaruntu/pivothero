package pivottable.core;


public enum AppState {
    IMPORTER, PIVOTTABLE, PHDEXPORT, PDFEXPORT
}
