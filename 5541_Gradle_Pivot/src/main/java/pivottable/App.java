package pivottable;

import javafx.application.Application;
import javafx.stage.Stage;
import pivottable.core.AppState;
import pivottable.io.ImporterFactory;
import pivottable.io.PHDExporter;
import pivottable.io.PDFExporter;
import pivottable.model.PivotTable;
import pivottable.ui.ImporterUI;
import pivottable.ui.PivotTableUI;


/**
 * The main class in the Pivot Hero application.
 */
public class App extends Application {

    /* Class attributes */

    /**
     * Manages state of app
     */
    private AppState state;

    /**
     * Root stage for GUI
     */
    private Stage rootStage;

    /**
     * Application's PivotTable instance
     */
    private PivotTable pt;
    
    /**
     * Application's Importer Factory
     */
    private ImporterFactory importerFactory;
    
    /**
     * Boolean flag to set if restoring previously saved pivotTable
     */
    private boolean pivotInit;

    /**
     * ImporterUI importUI
     */
    private ImporterUI importUI;

    /**
     * PivotTable UI
     */
    private PivotTableUI ptUI;

    /* Main Method */

    /**
     * Main entry-point for the application
     * 
     * @param args
     */
    public static void main(String[] args) {
    	
        // launch the application
        Application.launch( App.class, (java.lang.String[]) null );

        // exit when done
        System.exit( 0 );

    }

    /* Class methods */

    /**
     * Launched by Application
     */
    @Override
    public void start(Stage primaryStage) throws Exception {

        // initialize rootStage
        rootStage = primaryStage;

        // initialize pt
        pt = new PivotTable();
        
        // intialize importer factory
        importerFactory = new ImporterFactory();
        
        // set pivotInit to false
        pivotInit = false;

        // set state to importing
        state = AppState.IMPORTER;

        // launch the importer dialog
        setState( AppState.IMPORTER );

    }

    /**
     * Manages different states of application
     */
    public void setState(AppState state) {
        if ( state == AppState.IMPORTER ) {
            // launch the importer dialog
            this.state = state;
            rootStage.centerOnScreen();
            importUI = new ImporterUI( this );
            importUI.drawUI();
        } else if ( state == AppState.PIVOTTABLE ) {
            this.state = state;
            // launch the pivotTable UI
            ptUI = new PivotTableUI( this );
            ptUI.drawUI(pivotInit);
            pivotInit = false;
        } else if ( state == AppState.PHDEXPORT ) {
            // don't set or call state since it returns to PIVOTTABLE
            // Create exporter
            PHDExporter phdExporter = new PHDExporter( this );
            phdExporter.drawUI();
        } else if (state == AppState.PDFEXPORT) {
        	PDFExporter pdfExporter = new PDFExporter(this);
        	pdfExporter.drawUI();
        }
    }

    /**
     * @return the rootStage
     */
    public Stage getRootStage() {
        return rootStage;
    }

    /**
     * @param rootStage the rootStage to set
     */
    public void setRootStage(Stage rootStage) {
        this.rootStage = rootStage;
    }

    /**
     * @return the pt
     */
    public PivotTable getPivotTable() {
        return pt;
    }

    /**
     * @param pt the pt to set
     */
    public void setPivotTable(PivotTable pt) {
        this.pt = pt;
    }

    /**
     * @return the state
     */
    public AppState getState() {
        return state;
    }

    /**
     * @return the pivotInit
     */
    public boolean isPivotInit() {
        return pivotInit;
    }

    /**
     * @param pivotInit the pivotInit to set
     */
    public void setPivotInit(boolean pivotInit) {
        this.pivotInit = pivotInit;
    }

    /**
     * @return the importerFactory
     */
    public ImporterFactory getImporterFactory() {
        return importerFactory;
    }

}
