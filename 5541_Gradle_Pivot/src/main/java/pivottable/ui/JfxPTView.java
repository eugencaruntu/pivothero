package pivottable.ui;

import java.util.ArrayList;

import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import pivottable.model.PivotTable;
import pivottable.model.THTLabel;

/**
 * JfxPTView draws a pivot table to the console.
 */
public class JfxPTView implements PTViewInterface {

    /* Class attributes */

    private PivotTable model;

    private GridPane gridPane;

    /* Constructors */

    public JfxPTView( PivotTable model ) {
        this.model = model;
        this.gridPane = new GridPane();
        this.gridPane.setHgap( 1 );
        this.gridPane.setVgap( 1 );
    }

    /**
     * Returns the JFX view instance -- a GridPane
     * 
     * @return
     */
    public GridPane getView() {
        return this.gridPane;
    }

    /**
     * Draws the PivotTable
     */
    @Override
    public void drawPivotTable() {

        /* Get Paging parameters from model */
        int colStart = model.getPageColStart();
        int colSpan = model.getColsPerPage();
        int rowStart = model.getPageRowStart();
        int rowSpan = model.getRowsPerPage();
        int colMax = model.getTableColSize();
        int rowMax = model.getTableRowSize();

        /* Must clear the gridPane contents or new values are drawn over */
        this.gridPane.getChildren().clear();

        /* How many columns are actually being printed in the view */
        int cSpan = 0;
        if ( colStart + colSpan > colMax ) {
            cSpan = colMax - colStart;
        }
        else {
            cSpan = colSpan;
        }

        /**
         * Print column headers
         */

        /* Transform row headers into table that include spans for segments */
        ArrayList<ArrayList<String>> rowHeaderTable = createRowHeaderTable();

        /* For printing blank space to left of column headers */
        int rowHeaderWidth = rowHeaderTable.size();

        // Turn header structure into array
        String[][] colHeaderArray =
                new String[model.getColHeadStruct().size() - 1][colMax];

        // Create array for header
        for ( int i = 1; i < model.getColHeadStruct().size(); i++ ) {

            int rowCount = 0;

            for ( THTLabel label : model.getColHeadStruct().get( i ) ) {
                for ( int j = 0; j < label.getSpan(); j++ ) {
                    if ( j == 0 ) {
                        colHeaderArray[i - 1][rowCount] = label.getLabel();
                    }
                    else {
                        colHeaderArray[i - 1][rowCount] = "";
                    }
                    rowCount++;
                }
            }
        }

        // Print the header table for the column fields and segments
        int colHeaderHeight = colHeaderArray.length;

        // number of ROWS in column header
        for ( int i = 0; i < colHeaderHeight; i++ ) {

            int spanCounter = 1;
            int firstSpan = 0;

            // check first label for row
            if ( colHeaderArray[i][colStart] == "" ) {

                // label is blank, go backwards through array to get label
                int newJ = colStart;
                while ( (colHeaderArray[i][newJ] == "") && (newJ - 1 >= 0) ) {
                    newJ--;
                }

                // is the next cell empty?
                while ( (spanCounter < cSpan)
                        && (colHeaderArray[i][colStart + spanCounter] == "") ) {
                    spanCounter++; // increase the span of the cell
                    firstSpan++;
                }

                this.gridPane.add( createCell( colHeaderArray[i][newJ], "pt-header-cell",
                        "pt-white-text" ), rowHeaderWidth, i, spanCounter, 1 );

                // reset spanCounter
                spanCounter = 1;

            }
            else {

                // is the next cell empty?
                while ( (spanCounter < cSpan)
                        && (colHeaderArray[i][colStart + spanCounter] == "") ) {
                    spanCounter++; // increase the span of the cell
                    firstSpan++;
                }

                this.gridPane.add( createCell( colHeaderArray[i][colStart],
                        "pt-header-cell", "pt-white-text" ), rowHeaderWidth, i,
                        spanCounter, 1 );

                // reset spanCounter
                spanCounter = 1;
            }

            // for the other labels
            for ( int j = colStart + 1 + firstSpan; j < colStart + cSpan; j++ ) {

                // is the next cell empty?
                while ( (spanCounter + j - colStart < cSpan)
                        && (colHeaderArray[i][j + spanCounter] == "") ) {
                    spanCounter++;
                }

                // add label to grid
                this.gridPane.add(
                        createCell( colHeaderArray[i][j], "pt-header-cell",
                                "pt-white-text" ),
                        j + rowHeaderWidth - colStart, i, spanCounter, 1 );

                // skip the next spanCounter cells since they're blank
                j += spanCounter - 1;

                // reset spanCounter
                spanCounter = 1;

            }
        }

        // Print total header
        this.gridPane.add( createCell( "Row Totals", "pt-cell-total" ),
                cSpan + rowHeaderWidth + 1, colHeaderHeight - 1 );

        /**
         * Print the row headers and cells
         */

        /* How many columns are actually being printed in the view */
        int rSpan = 0;
        if ( rowStart + rowSpan > rowMax ) {
            rSpan = rowMax - rowStart;
        }
        else {
            rSpan = rowSpan;
        }

        for ( int i = rowStart; i < rSpan + rowStart; i++ ) {
            // Print row labels
            for ( int j = 0; j < rowHeaderWidth; j++ ) {

                int spanCounter = 1;

                if ( i == rowStart ) {
                    // check first label for blank
                    if ( (rowHeaderTable.get( j ).get( i ).length() == 0) && (i > 0) ) {
                        // label is blank, go backwards through array to get
                        // label
                        int newI = i;
                        while ( (rowHeaderTable.get( j ).get( newI ).length() == 0)
                                && (newI - 1 >= 0) ) {
                            newI--;
                        }

                        // is the next cell empty?
                        while ( (spanCounter < rSpan) && (rowHeaderTable.get( j )
                                .get( i + spanCounter ) == "") ) {
                            spanCounter++; // increase the span of the cell
                        }

                        this.gridPane.add(
                                createCell( rowHeaderTable.get( j ).get( newI ),
                                        "pt-header-cell-vertical", "pt-white-text" ),
                                j, i + colHeaderHeight - rowStart, 1, spanCounter );
                    }
                    else {

                        // is the next cell empty?
                        while ( (spanCounter < rSpan) && (rowHeaderTable.get( j )
                                .get( i + spanCounter ) == "") ) {
                            spanCounter++; // increase the span of the cell
                        }

                        this.gridPane.add(
                                createCell( rowHeaderTable.get( j ).get( i ),
                                        "pt-header-cell-vertical", "pt-white-text" ),
                                j, i + colHeaderHeight - rowStart, 1, spanCounter );
                    }
                }
                else {

                    String value = rowHeaderTable.get( j ).get( i );

                    if ( value == "" ) {
                        continue;
                    }

                    // is the next cell empty?
                    while ( (spanCounter < (rSpan - (i - rowStart)))
                            && (rowHeaderTable.get( j ).get( i + spanCounter ) == "") ) {
                        spanCounter++; // increase the span of the cell
                    }

                    this.gridPane.add(
                            createCell( value, "pt-header-cell-vertical",
                                    "pt-white-text" ),
                            j, i + colHeaderHeight - rowStart, 1, spanCounter );
                }
            }

            // Print rows of PivotCells
            for ( int k = colStart; k < cSpan + colStart; k++ ) {
                this.gridPane.add( createCell( model.getCellAt( i, k ), "pt-cell" ),
                        k + rowHeaderWidth - colStart, i + colHeaderHeight - rowStart );
            }

            // Print row total
            this.gridPane.add(
                    createCell( model.getTotalsRow().get( i ).getValue(),
                            "pt-cell-total" ),
                    cSpan + rowHeaderWidth + 1, i + colHeaderHeight - rowStart );

        }

        // Print total header
        this.gridPane.add( createCell( "Column Totals", "pt-cell-total" ), rowHeaderWidth - 1,
                rSpan + colHeaderHeight );

        // Print column totals
        for ( int l = colStart; l < cSpan + colStart; l++ ) {
            this.gridPane.add(
                    createCell( model.getTotalsCol().get( l ).getValue(),
                            "pt-cell-total" ),
                    l + rowHeaderWidth - colStart, rSpan + colHeaderHeight );
        }

        // Print total totals

        this.gridPane.add(
                createCell( model.getSumTotal().getValue(), "pt-cell-total-total",
                        "pt-white-text" ),
                cSpan + rowHeaderWidth + 1, rSpan + colHeaderHeight);

    }

    /**
     * Creates the structure needed to display the header table
     * 
     * @return 2D ArrayList containing row labels with spans
     */
    private ArrayList<ArrayList<String>> createRowHeaderTable() {

        ArrayList<ArrayList<String>> rowHeaderTable = new ArrayList<>();

        // For each row (rowHeadStruct is 1-indexed / doesn't show root)
        for ( int i = 1; i < model.getRowHeadStruct().size(); i++ ) {

            // Instantiate new ArrayList to hold row
            rowHeaderTable.add( new ArrayList<String>() );

            // For each label in the row
            for ( THTLabel label : model.getRowHeadStruct().get( i ) ) {

                // Format the label for consistent spacing

                /*
                 * Push the formatted label to the row. Uses i - 1 since
                 * rowHeaderArrayList is 0 indexed
                 */
                rowHeaderTable.get( i - 1 ).add( label.getLabel() );

                // Get the row span for the label
                int span = label.getSpan();

                // Fill the remaining span with whitespace
                for ( int j = 1; j < span; j++ ) {
                    // Uses i - 1 since rowHeaderArrayList is 0 indexed
                    // IMPORTANT must start with space
                    rowHeaderTable.get( i - 1 ).add( "" );
                }
            }
        }
        return rowHeaderTable;
    }

    /**
     * Method that creates the individual table cells and sets css style
     */
    private HBox createCell(String label, String cssClass) {
        return createCell( label, cssClass, "pt-text" );
    }

    /**
     * Method that creates the individual table cells and sets css style for
     * container and text inside.
     */
    private HBox createCell(String label, String cssClass, String textCssClass) {
        HBox hbox = new HBox();
        hbox.getStyleClass().add( cssClass );
        Text text = new Text( label );
        text.getStyleClass().add( textCssClass );
        hbox.getChildren().add( text );
        hbox.setPrefWidth( 160 );
        return hbox;
    }

}
