package pivottable.ui;

import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.CheckComboBox;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import pivottable.App;
import pivottable.core.AppState;
import pivottable.model.PTField;
import pivottable.model.PTFieldSet;
import pivottable.model.PTFieldValue;
import pivottable.model.PTOperation;
import pivottable.model.PTSortOrder;
import pivottable.model.PivotTable;

/**
 * JavaFX view for the PivotTableUI
 */
public class PivotTableUI {

	/* Class constants */

	private final int DEFAULT_COLS_PER_PAGE = 6;
	private final int DEFAULT_ROWS_PER_PAGE = 12;

	/* Class attributes */

	/**
	 * The stage and its controls
	 */
	final DirectoryChooser folderChooser = new DirectoryChooser();
	final Menu fileMenu = new Menu( "File" );
	final Menu helpMenu = new Menu( "Help" );
	private ArrayList<String> labels = new ArrayList<String>();
	private ArrayList<PTField> fields = new ArrayList<PTField>();
	private ObservableList<String> avlblabels = FXCollections.observableArrayList();
	private Button nextPage = new Button( "Next Page" );
	private Button prevPage = new Button( "Previous Page" );
	private Button submit = new Button( "Submit" );
	private ComboBox<PTOperation> opsSelector = new ComboBox<PTOperation>();
	private ComboBox<String> filterField = new ComboBox<String>();
	private Label columnLabel = new Label( "Columns:" );
	private Label fieldsLabel = new Label( "Available Fields:" );
	private Label filterLabel = new Label( "Filter by:" );
	private Label opsLabel = new Label( "Operation:" );
	private Label rowLabel = new Label( "Rows:" );
	private Label valueLabel = new Label( "Aggregation Field:" );
	private Label sortCLabel = new Label( "Sort:" );
	private Label sortRLabel = new Label( "Sort:" );
	private Label colsPerPageLabel = new Label( "Page Columns:");
	private Label rowsPerPageLabel = new Label( "Page Rows: ");
	private Label currentPageLabel = new Label( "Current page: ");
	private ListView<String> fieldsSelector = new ListView<String>();
	private CheckComboBox<String> filterSelector = new CheckComboBox<String>();
	private ListView<String> columnSelector = new ListView<String>();
	private ListView<String> rowSelector = new ListView<String>();
	private ListView<String> colSrt = new ListView<String>();
	private ListView<String> rowSrt = new ListView<String>();
	private ListView<String> valueSelector = new ListView<String>();
	private MenuBar menuBar = new MenuBar();
	private MenuItem importNewData = new MenuItem( "Import new data..." );
	private MenuItem exportPDF = new MenuItem( "Export as PDF..." );
	private MenuItem savePHD = new MenuItem( "Save as Pivot Hero file..." );
	private Scene pivotTableScene;
	private Stage primaryStage;
	private Text totalPages = new Text();
	private TextArea PTbox = new TextArea();
	private TextField colsPerPage = new TextField();
	private TextField rowsPerPage = new TextField();
	private TextField currentPage = new TextField();
	private VBox options = new VBox();

	/**
	 * The application
	 */
	private App app;

	/**
	 * The current pivot table instance
	 */
	PivotTable pt;

	/**
	 * The controller
	 */
	PTController ptController;

	/**
	 * The JfxPTView
	 */
	JfxPTView view;

	/* Constructors */

	/**
	 * Constructor
	 */
	public PivotTableUI( App app ){
		this.app = app;
		pt = app.getPivotTable();
		primaryStage = app.getRootStage();
		// Create the view
		view = new JfxPTView( pt );
		// Create the controller
		ptController = new PTController( pt, view );    
	}

	/**
	 * Defines and draws all the controls for the Importer Dialog
	 */
	public void drawUI() {
		drawUI(false);
	}

	/**
	 * Defines and draws all the controls for the Importer Dialog
	 * @param init set to true if you want to drawUI and init
	 * @throws Exception
	 */
	public void drawUI( boolean init ) {

		// MENU BAR
		importNewData.setOnAction( e -> { app.setState( AppState.IMPORTER ); });
		
		savePHD.setOnAction( e -> { app.setState( AppState.PHDEXPORT ); });
		
		exportPDF.setOnAction( e -> {
			if (app.isPivotInit()) app.setState( AppState.PDFEXPORT );
			else {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setHeaderText("Nothing to save.");
				alert.setContentText("Please configure and create a pivot table first.");
				alert.showAndWait();
			}
		} );

		// Collect available operations/aggregations
		ObservableList<PTOperation> ops = FXCollections.observableArrayList( PTOperation.values() );

		// Labels and fields (are used later to retrieve original index)
		labels =  pt.getPTData().getFieldLabels();

		// The list of available fields from the pivot data (observable list
		// will lose the index once DragAndDrop is done)
		avlblabels = FXCollections.observableArrayList( labels );

		// FIELD SELECTOR
		fieldsSelector.setMaxWidth(300);
		fieldsSelector.setMinWidth(300);
		fieldsSelector.setMinHeight(60);
		fieldsSelector.setMaxHeight( 135 );
		fieldsSelector.setItems( avlblabels );
		pivottable.ui.DragAndDrop.dragDrop( fieldsSelector );
		renameField(fieldsSelector);


		// ROWS
		pivottable.ui.DragAndDrop.placeholder( rowSelector );
		pivottable.ui.DragAndDrop.dragDrop( rowSelector );
		rowSelector.setOnDragDropped( e -> addSort( rowSelector, rowSrt ));	
		rowSelector.setOnDragDone(e -> removeSRT());
		renameField(rowSelector);

		// COLUMNS
		pivottable.ui.DragAndDrop.placeholder( columnSelector );
		pivottable.ui.DragAndDrop.dragDrop( columnSelector );
		columnSelector.setOnDragDropped( e -> addSort( columnSelector, colSrt ));
		columnSelector.setOnDragDone(e -> removeSRT());
		renameField(columnSelector);

		//SORT for rows and columns
		rowSrt.setMaxWidth(50);
		colSrt.setMaxWidth(50);
		rowSrt.setMinWidth(50);
		colSrt.setMinWidth(50);
		rowSrt.setOnMouseClicked( e -> changeMe(rowSrt) );
		colSrt.setOnMouseClicked( e -> changeMe(colSrt) );

		// VALUES
		pivottable.ui.DragAndDrop.placeholder( valueSelector );
		pivottable.ui.DragAndDrop.dragDrop( valueSelector );
		valueSelector.setOnDragExited(e -> putBack( valueSelector ));
		renameField(valueSelector);

		// FILTER
		filterField.setMaxWidth(140);
		filterField.setMinWidth(140);
		filterField.getItems().setAll( avlblabels );
		filterField.getItems().add(0, "");
		filterField.getSelectionModel().selectedItemProperty().addListener( e -> getFilterVals());
		//values to populate filter by once field is selected
		filterSelector.setMaxWidth(140);
		filterSelector.setMinWidth(140);

		// OPERATIONS
		opsSelector.getItems().setAll( ops );
		opsSelector.setMaxWidth(140);
		opsSelector.setMinWidth(140);

		// placeholder forPivot Table output
		PTbox.setFont( Font.font( "Courier New", 11.0 ) );

		// Next Page button
		nextPage.setOnAction( e -> getNextPage() );
		nextPage.setDisable( true );

		// Previous Page button
		prevPage.setOnAction( e -> getPreviousPage() );
		prevPage.setDisable( true );

		// Text area for cols per page
		colsPerPage.setText( Integer.toString( DEFAULT_COLS_PER_PAGE ) );
		rowsPerPage.setText( Integer.toString( DEFAULT_ROWS_PER_PAGE ) );
		currentPage.setText( Integer.toString( pt.getCurrentPage() ) );
		currentPage.setMaxWidth( 40 );
		totalPages.setText( "" );

		HBox currentTotalPages = new HBox();
		currentTotalPages.getChildren().addAll( currentPage, totalPages );
		currentTotalPages.setMinWidth( 90 );
		currentTotalPages.setSpacing( 10 );
		currentTotalPages.setAlignment(Pos.BASELINE_LEFT);

		// Submit button
		submit.getStyleClass().add("pt-submit");
		submit.setOnAction( e -> goSubmit() );
		submit.setMinWidth( 90 );

		//===================================================================
		//========                   ASEMBLER                    ============
		//===================================================================

		// menu bar
		fileMenu.getItems().addAll( importNewData, savePHD, exportPDF );
		menuBar.getMenus().addAll( fileMenu );

		// pivot controls on right side
		options.setSpacing( 5 );
		options.getChildren().addAll( filterLabel, filterField, filterSelector );
		options.setMaxWidth(140);
		options.setMinWidth(140);

		VBox options2 = new VBox();
		options2.setSpacing( 5 );
		options2.getChildren().addAll( opsLabel, opsSelector );
		options2.setMaxWidth(140);
		options2.setMinWidth(140);

		valueSelector.setMinHeight( 20 );
		valueSelector.setMaxHeight( 20 );
		VBox aggregationValue = new VBox();
		aggregationValue.setSpacing( 15 );
		aggregationValue.getChildren().addAll( valueLabel, valueSelector );
		aggregationValue.setMaxWidth(300);
		aggregationValue.setMinWidth(300);

		HBox col = new HBox();
		col.setSpacing( 0 );
		col.setMinHeight(60);
		col.setMaxHeight(60);
		col.getChildren().addAll( columnSelector, colSrt );
		HBox row = new HBox();
		row.setSpacing( 0 );
		row.setMinHeight(60);
		row.setMaxHeight(60);
		row.getChildren().addAll( rowSelector, rowSrt );

		HBox cSrt = new HBox();
		cSrt.setMaxWidth(300);
		cSrt.setMinWidth(300);
		cSrt.setSpacing( 65 );
		columnLabel.setMinWidth( 190 );
		columnLabel.setMaxWidth( 190 );
		sortCLabel.setMinWidth( 50 );
		cSrt.getChildren().addAll( columnLabel, sortCLabel );
		HBox rSrt = new HBox();
		rSrt.setMaxWidth(300);
		rSrt.setMinWidth(300);
		rSrt.setSpacing( 65 );
		rowLabel.setMinWidth( 190 );
		rowLabel.setMaxWidth( 190 );
		sortRLabel.setMinWidth( 50 );
		rSrt.getChildren().addAll( rowLabel, sortRLabel );

		GridPane grid = new GridPane();
		grid.setHgap( 20 );
		grid.setVgap( 15 );
		grid.setMaxSize( 300, 340 );
		grid.setMinSize( 300, 300 );
		grid.add( options2, 0, 0, 1, 1 );
		grid.add( options, 1, 0, 1, 1 );
		grid.add( cSrt, 0, 1, 2, 1 );
		grid.add( col, 0, 2, 2, 1);
		grid.add( rSrt, 0, 3, 2, 1);
		grid.add( row, 0, 4, 2, 1);
		grid.add( aggregationValue, 0, 5, 2, 1 );

		GridPane paging = new GridPane();
		paging.setMaxWidth( 300 );
		paging.setVgap( 5 );
		paging.setHgap( 5 );
		paging.add( colsPerPageLabel, 0, 0 );
		paging.add( rowsPerPageLabel, 1, 0 );
		paging.add( currentPageLabel, 2, 0 );
		paging.add( colsPerPage, 0, 1 );
		paging.add( rowsPerPage, 1, 1 );
		paging.add( currentTotalPages, 2, 1 );

		HBox buttons = new HBox();
		buttons.setSpacing( 10 );
		buttons.getChildren().addAll( submit, prevPage, nextPage );

		// schema Box
		VBox control = new VBox();
		control.setSpacing( 20 );
		control.setMinWidth( 350 );
		control.setMaxWidth( 350 );
		control.setPadding( new Insets( 20, 20, 20, 20 ) );
		control.getChildren().addAll( fieldsLabel, fieldsSelector, grid, paging, buttons );

		// temporarily split left side of split pane into top which has JfxPTView and 
		// bottom which has ConsolePTView

		VBox tableContainer = new VBox();
		tableContainer.setSpacing( 0 );
		tableContainer.getStyleClass().add( "pt-table-bg" );
		tableContainer.setPadding( new Insets( 20, 20, 20, 20 ) );
		tableContainer.getChildren().addAll( this.view.getView() );

		SplitPane splitPane = new SplitPane();
		splitPane.getItems().addAll( tableContainer, control );
		splitPane.setDividerPositions( 0.75f );

		BorderPane borderPane = new BorderPane();
		borderPane.setTop( menuBar );
		borderPane.setCenter( splitPane );

		// scene properties and display
		pivotTableScene = new Scene( borderPane, 1400, 800 );
		pivotTableScene.getStylesheets().add("application.css");

		primaryStage.setScene( pivotTableScene );
		primaryStage.centerOnScreen();
		primaryStage.setMaximized( true );
		primaryStage.setTitle( "PivotHero" );

		// if init is set to true, restore session
		if (init) {
			ptController.drawPage();
			nextPage.setDisable( false );
			prevPage.setDisable( false );
			
			//remove placeholder and restore columns, rows, aggregation field
			// at the same time remove from fieldsSelector
			columnSelector.getItems().clear();
			rowSelector.getItems().clear();
			valueSelector.getItems().clear();

			pt.getColumnFields().forEach( t -> {
				columnSelector.getItems().add(pt.getPTData().getFieldLabel(t.getFieldIndex()) );
				fieldsSelector.getItems().remove(pt.getPTData().getFieldLabel(t.getFieldIndex()));
			} );

			pt.getRowFields().forEach( t -> {
				rowSelector.getItems().add(pt.getPTData().getFieldLabel(t.getFieldIndex()) );
				fieldsSelector.getItems().remove(pt.getPTData().getFieldLabel(t.getFieldIndex()));
			} );

			valueSelector.getItems().add(pt.getPTData().getFieldLabel(pt.getAggregationField()));
			fieldsSelector.getItems().remove(pt.getPTData().getFieldLabel(pt.getAggregationField()));


			// restore operation
			opsSelector.getSelectionModel().select(pt.getOperationType());

			//restore sorting
			pt.getColumnFields().forEach( t -> colSrt.getItems().add( t.getSortOrder().toString())  );
			pt.getRowFields().forEach( t -> rowSrt.getItems().add( t.getSortOrder().toString())  );

			// restore filter
			labels = pt.getPTData().getFieldLabels();
			avlblabels = FXCollections.observableArrayList( labels );
			filterField.getItems().setAll( avlblabels );
			filterField.getItems().add(0, "");

			if (pt.getFilterFieldValues().size() > 0 ){
				// get the filed name that does the filtering
				PTFieldValue filterValue = pt.getFilterFieldValues().get(0);
				filterField.getSelectionModel().select(pt.getPTData().getFieldLabel(filterValue.getFieldIndex()));

				// check filter selections 
				for (int i=0; i < filterValue.getFieldValues().length; i++)
					filterSelector.getCheckModel().check(pt.getFilterFieldValues().get(0).getFieldValues()[i]);
			}
			
		}
		primaryStage.show();
		primaryStage.setOnCloseRequest( e -> System.exit( 0 ) );

	}

	//===================================================================
	//========                EVENT METHODS                  ============
	//===================================================================

	/**
	 * Populate the items once field for filtering is selected
	 */
	private void getFilterVals() {
		String sel = filterField.getSelectionModel().getSelectedItem();
		filterSelector.getCheckModel().clearChecks();
		filterSelector.getItems().clear();

		if (sel != ""){
			ObservableList<String> filterlabels = FXCollections.observableArrayList( 
					pt.getPTData().getFieldValuesAlpha(getPTDataIndex(sel), PTSortOrder.ASC) );
			filterSelector.getItems().addAll(filterlabels);
		}
	}

	/**
	 * Add to sort ListView if needed
	 * @param x is the ListView selector triggering this method
	 * @param s is the ListView containing sort information
	 */
	private void addSort(ListView<String> x, ListView<String> s){
		if (x.getItems().size() > s.getItems().size()){
			s.getItems().add("ASC");
		}
	}

	/**
	 * put back first element from value into fieldsSelector
	 * only allow one value in field
	 * @param x is the ListView selector triggering this method
	 */
	private void putBack(ListView<String> x){
		if (x.getItems().size() > 1){
			fieldsSelector.getItems().add( x.getItems().get( 0 ) );
			x.getItems().remove(0);
		}
	}

	/**
	 * toggle sort order
	 */
	private void changeMe(ListView<String> x) {
		if (x.getItems().size() > 0 && x.getSelectionModel().getSelectedItem() != null) {
			if (x.getSelectionModel().getSelectedItem().matches("") || x.getSelectionModel().getSelectedItem().contains("DSC"))
				x.getItems().set(x.getSelectionModel().getSelectedIndex(), "ASC");

			else if (x.getSelectionModel().getSelectedItem().contains("ASC"))
				x.getItems().set(x.getSelectionModel().getSelectedIndex(), "DSC");

			removeSRT();
		}
	}

	/**
	 * Remove from sort ListView if needed
	 */
	private void remSort(ListView<String> x, ListView<String> s) {
		if ( x.getItems().size() > 0 && s.getItems().size()> 0 ){
			int size = s.getItems().size();
			while (size > x.getItems().size() && size > 1){
				s.getItems().remove( size - 1 );
				size--;
			}

			if (x.getItems().get(0) == "Drag fields here"){
				s.getItems().remove(0);
			}
		}
	}

	/**
	 * remove sort order on field removal
	 */
	private void removeSRT(){
		remSort( columnSelector, colSrt );
		remSort( rowSelector, rowSrt );
	}


	/**
	 * Submit options once validated
	 */
	private void goSubmit(){
		// Don't let user submit with empty fields
		if ( !(columnSelector.getItems().isEmpty()
				|| columnSelector.getItems().get( 0 ) == "Drag fields here")
				&& !(rowSelector.getItems().isEmpty()
						|| rowSelector.getItems().get( 0 ) == "Drag fields here")
				&& !(valueSelector.getItems().isEmpty()
						|| valueSelector.getItems().get( 0 ) == "Drag fields here")
				&& (opsSelector.getValue() != null) ) {

			PTbox.setText( "" );

			// Create the column header tree
			ArrayList<PTFieldSet> columnFields = new ArrayList<PTFieldSet>();
			columnSelector.getItems().forEach( s -> buildHeaderTree(columnSelector, s, colSrt, columnFields) );

			// Create the row header tree
			ArrayList<PTFieldSet> rowFields = new ArrayList<PTFieldSet>();
			rowSelector.getItems().forEach( s -> buildHeaderTree(rowSelector, s, rowSrt, rowFields) );

			// get the aggregation (only take the first value in the value fields)
			int valueFields = getPTDataIndex(valueSelector.getItems().get( 0 ));
			PTOperation operation = opsSelector.getValue();

			// get the filter and selected values
			ArrayList<PTFieldValue> filters = new ArrayList<>();
			int index = labels.indexOf(filterField.getSelectionModel().getSelectedItem());
			List<String> selection = filterSelector.getCheckModel().getCheckedItems();
			String[] values = selection.toArray(new String[selection.size()]);
			// if the filterField is empty
			if ( (index >= 0) && (values.length > 0) ){ 
				filters.add( new PTFieldValue( index, values ) );   
			}

			// update values
			ptController.setColumnFields( columnFields );
			ptController.setRowFields( rowFields );
			ptController.setAggregationField( valueFields );
			ptController.setOperationType( operation );
			ptController.setFilterFieldValues(filters);
			setPaging();
			// Draw the pivot table
			ptController.drawPage();

			submit.setText( "Update" );
			prevPage.setDisable( false );
			nextPage.setDisable( false );

			// update controls
			colsPerPage.setText( Integer.toString( pt.getColsPerPage() ) );
			rowsPerPage.setText( Integer.toString( pt.getRowsPerPage() ) );
			currentPage.setText( Integer.toString( pt.getCurrentPage() ) );
			totalPages.setText( "/ " + Integer.toString( pt.getTotalNumPages() ) );
			app.setPivotInit(true);
		}
		else {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText("Missing Parameters");
			alert.setContentText("Please set at least one value for Operation, ColumnFields, RowFields, and Aggregation Field.");
			alert.showAndWait();
		}
	}

	/** 
	 * get the index of the field based on the label
	 */
	private int getPTDataIndex(String label){
		// get the fields
		fields = pt.getPTData().getFields();
		int fldIdx = -1;
		// Get the index of s from fields by iterating through fields and comparing label string
		for ( int i = 0; i < fields.size(); i ++) {
			if (fields.get( i ).getLabel().equals( label )) {
				// set s to the index in fields
				fldIdx = i;
				break;
			}
		}
		return fldIdx;
	}

	/**
	 * 
	 * build the header trees for each list view
	 */
	private ArrayList<PTFieldSet> buildHeaderTree(ListView<String> listView, String label, ListView<String> srtListView, ArrayList<PTFieldSet> fieldsArray) {
		int srtIdx = listView.getItems().indexOf( label );
		PTSortOrder srt = PTSortOrder.valueOf( srtListView.getItems().get(srtIdx) );
		fieldsArray.add( new PTFieldSet( getPTDataIndex(label), srt ) );
		return fieldsArray;
	}


	/**
	 * Sets paging on update / next / prev page
	 */
	private void setPaging() {

		// get the paging parameters

		int colsPerPageValue = DEFAULT_COLS_PER_PAGE;
		// if it's not an integer use default
		if ( colsPerPage.getText().trim() != "") {
			try {
				colsPerPageValue = Integer.parseInt( colsPerPage.getText() );
			} catch (Exception e) {}
		}

		int rowsPerPageValue = DEFAULT_ROWS_PER_PAGE;

		// if it's not an integer use default
		if ( rowsPerPage.getText().trim() != "") {
			try {
				rowsPerPageValue = Integer.parseInt( rowsPerPage.getText() );
			} catch (Exception e) {}
		}

		int currentPageValue = pt.getCurrentPage();

		// if it's not an integer use default
		if ( currentPage.getText().trim() != "") {
			try {
				currentPageValue = Integer.parseInt( currentPage.getText() );
			} catch (Exception e) {}
		}

		ptController.setColsPerPage( colsPerPageValue );
		ptController.setRowsPerPage( rowsPerPageValue );
		ptController.setCurrentPage( currentPageValue );

	}

	/**
	 * Gets the next page
	 */
	private void getNextPage (){  
		// Get page parameters from pivotTable
		int currentPageValue = pt.getCurrentPage();
		int totalNumPages = pt.getTotalNumPages();
		// Check bounds
		if ( (currentPageValue + 1) > totalNumPages ) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText("No more pages");
			alert.showAndWait();
		} else {
			ptController.setCurrentPage( currentPageValue + 1 );
			// update controls
			colsPerPage.setText( Integer.toString( pt.getColsPerPage() ) );
			rowsPerPage.setText( Integer.toString( pt.getRowsPerPage() ) );
			currentPage.setText( Integer.toString( pt.getCurrentPage() ) );
			ptController.drawPage();
		} 
	}

	/**
	 * Gets the previous page
	 */
	private void getPreviousPage (){  
		// Get page parameters from pivotTable
		int currentPageValue = pt.getCurrentPage();
		// Check bounds
		if ( (currentPageValue - 1) <= 0 ) {
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setHeaderText("No previous pages");
			alert.showAndWait();
		} else {
			ptController.setCurrentPage( currentPageValue - 1 );
			colsPerPage.setText( Integer.toString( pt.getColsPerPage() ) );
			rowsPerPage.setText( Integer.toString( pt.getRowsPerPage() ) );
			currentPage.setText( Integer.toString( pt.getCurrentPage() ) );
			ptController.drawPage();
		} 
	}

	/**
	 * method to rename a field in a ListView associating a contextual menu on right click
	 */
	private void renameField(ListView<String> list) {
		/* Create context menu for right-click action on label */
		final ContextMenu contextMenu = new ContextMenu();
		MenuItem menuItem = new MenuItem("Rename Field...");
		contextMenu.getItems().add(menuItem);
		Button submit = new Button("Done");             
		Label nameLabel = new Label("Enter the new name:");

		TextField newName = new TextField();
		Stage dlg = new Stage();
		VBox box = new VBox();
		box.setSpacing(5);
		box.setPadding(new Insets(10, 10, 10, 10));
		box.getChildren().addAll(nameLabel, newName, submit);
		Scene dialog = new Scene(box, 160, 100);
		dlg.setScene(dialog);
		dlg.initStyle(StageStyle.UTILITY);
		dlg.initModality(Modality.APPLICATION_MODAL); 

		menuItem.setOnAction(e -> {
			newName.setText(list.getSelectionModel().getSelectedItem());
			dlg.showAndWait();
		});

		submit.setOnAction(e -> {
			if (newName.getText().length() > 0){
				int idx = list.getSelectionModel().getSelectedIndex();
				String oldLabel = list.getSelectionModel().getSelectedItem(); // get the old label before replacing it so we look for it in PTData
				String newLabel = newName.getText();
				if ( !list.getItems().contains(newLabel) || oldLabel.equals(newLabel) ){ // change only if value is unique
					list.getItems().set(idx, newLabel);
					filterField.getItems().set(getPTDataIndex(oldLabel) + 1, newLabel);
					pt.getPTData().setFieldLabel(getPTDataIndex(oldLabel), newLabel);
				}
				else {
					Alert alert = new Alert(AlertType.ERROR);
					alert.setHeaderText("There ia already a field with same name!");
					alert.setContentText("Please try to rename again using a diferent name for : " + oldLabel + "\nNo change will be done.");
					alert.showAndWait();
				}
			}
			dlg.close(); 
		});

		list.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (event.getButton().equals(MouseButton.SECONDARY)) {
					contextMenu.show(list, event.getScreenX(), event.getScreenY());
				}
			}
		});

	} // rename field ends


	//===================================================================

}
